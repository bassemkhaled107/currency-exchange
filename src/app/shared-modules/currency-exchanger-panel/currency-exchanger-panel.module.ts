import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";

import { MatSelectModule } from "@angular/material/select";
import { MatButtonModule } from "@angular/material/button";
import { MatSnackBarModule } from "@angular/material/snack-bar";

const MATERIALS = [MatSnackBarModule, MatSelectModule, MatButtonModule];

import * as fromContainers from "./containers";

@NgModule({
  declarations: [...fromContainers.CONTAINERS],
  imports: [CommonModule, ReactiveFormsModule, RouterModule, ...MATERIALS],
  exports: [fromContainers.CurrencyExchangerPanelComponent],
})
export class CurrencyExchangerPanelModule {}
