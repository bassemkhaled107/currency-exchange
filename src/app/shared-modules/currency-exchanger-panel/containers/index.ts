import { CurrencyExchangerPanelComponent } from "./currency-exchanger-panel/currency-exchanger-panel.component";

export const CONTAINERS = [CurrencyExchangerPanelComponent];

export { CurrencyExchangerPanelComponent } from "./currency-exchanger-panel/currency-exchanger-panel.component";
