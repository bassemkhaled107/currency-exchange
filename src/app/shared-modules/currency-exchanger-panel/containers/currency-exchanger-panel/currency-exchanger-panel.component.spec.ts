import { ComponentFixture, TestBed } from "@angular/core/testing";

import { CurrencyExchangerPanelComponent } from "./currency-exchanger-panel.component";

describe("CurrencyExchangerPanelComponent", () => {
  let component: CurrencyExchangerPanelComponent;
  let fixture: ComponentFixture<CurrencyExchangerPanelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CurrencyExchangerPanelComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrencyExchangerPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
