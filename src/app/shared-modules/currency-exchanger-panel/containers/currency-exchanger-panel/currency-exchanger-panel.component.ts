import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { Store, select } from "@ngrx/store";
import { Observable } from "rxjs";

import * as fromSharedStore from "src/app/shared-store";
import * as fromSharedActions from "src/app/shared-store/actions";
import * as fromSharedModels from "src/app/shared-models";
import { CustomValidators } from "src/app/utils/custom-validators";

@Component({
  selector: "app-currency-exchanger-panel",
  templateUrl: "./currency-exchanger-panel.component.html",
  styleUrls: ["./currency-exchanger-panel.component.scss"],
})
export class CurrencyExchangerPanelComponent implements OnInit, OnChanges {
  form: FormGroup;
  convertedAmount: number;
  convertedToCurrencyValue: number;
  isSubmitted: boolean = false;

  @Input() queryParams: {
    amount: string;
    from: string;
    to: string;
  };

  // used to send data to parent component when click on convert button
  @Output() CurrencyExchangeSubmitted = new EventEmitter<{
    baseCurrency: string;
    convertedToCurrency: string;
    amount: number;
  }>();

  get amountControl() {
    return this.form.get("amount");
  }
  get baseCurrencyControl() {
    return this.form.get("baseCurrency");
  }
  get convertedToCurrencyControl() {
    return this.form.get("convertedToCurrency");
  }

  currencyExchangeResult$: Observable<fromSharedModels.ICurrencyExchangeResponse>;
  currencies$: Observable<fromSharedModels.ICurrency[]>;

  constructor(
    private store: Store<fromSharedStore.IRootState>,
    private formBuilder: FormBuilder
  ) {
    this.form = this.formBuilder.group({
      amount: [1, [Validators.required, CustomValidators.isNegativeOrZero]],
      baseCurrency: ["USD"],
      convertedToCurrency: ["EUR"],
    });
  }

  ngOnInit(): void {
    this.store.dispatch(fromSharedActions.loadCurrencies());
    this.onCurrencyExchange();

    this.amountControl.valueChanges.subscribe((value) => {
      this.convertedAmount = 0;
      if (!value || !this.amountControl.valid) {
        this.baseCurrencyControl.disable();
        this.convertedToCurrencyControl.disable();
      } else if (this.queryParams?.from) {
        this.baseCurrencyControl.disable();
        this.convertedToCurrencyControl.enable();
      } else {
        this.baseCurrencyControl.enable();
        this.convertedToCurrencyControl.enable();
      }
    });

    this.currencyExchangeResult$ = this.store.pipe(
      select(fromSharedStore.getSharedCurrencyExchangeResult)
    );

    this.currencies$ = this.store.pipe(
      select(fromSharedStore.getCurrenciesInArray)
    );

    this.store
      .pipe(select(fromSharedStore.getSharedCurrencyExchangeResult))
      .subscribe((response) => {
        this.convertedToCurrencyValue =
          response?.data[this.convertedToCurrencyControl.value];
        this.convertedAmount =
          this.amountControl.value * this.convertedToCurrencyValue;
      });
  }

  ngOnChanges(changes: SimpleChanges): void {
    // check if there is query param then autofill the fields with the query params that shown in currency details module
    if (changes?.queryParams?.currentValue) {
      if (this.queryParams?.from) {
        this.convertedToCurrencyControl?.patchValue(this.queryParams.to);
        this.amountControl?.patchValue(this.queryParams.amount);
        this.baseCurrencyControl?.patchValue(this.queryParams.from);
        this.baseCurrencyControl?.disable();
        // this.onCurrencyExchange();
      }
    }
  }

  onChangeSelectedCurrency() {
    this.onCurrencyExchange();
  }

  // swap between the two currencies
  onSwapCurrencies() {
    const convertedToCurrency = this.convertedToCurrencyControl.value;
    this.convertedToCurrencyControl.patchValue(this.baseCurrencyControl.value);
    this.baseCurrencyControl.patchValue(convertedToCurrency);
    this.onCurrencyExchange();
  }

  // convert currency
  onCurrencyExchange(): void {
    if (this.form.valid) {
      // used to send data to parent component when click on convert button
      this.CurrencyExchangeSubmitted.emit({
        baseCurrency: this.baseCurrencyControl.value,
        convertedToCurrency: this.convertedToCurrencyControl.value,
        amount: this.amountControl.value,
      });

      this.store.dispatch(
        fromSharedActions.currencyExchange({
          payload: {
            baseCurrency: this.baseCurrencyControl.value,
            convertToCurrency: this.convertedToCurrencyControl.value,
          },
        })
      );
    }
  }
}
