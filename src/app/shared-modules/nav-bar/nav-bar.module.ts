import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";

import * as fromContainers from "./containers/";

@NgModule({
  declarations: [...fromContainers.CONTAINERS],
  imports: [CommonModule, RouterModule],
  exports: [fromContainers.NavBarComponent],
})
export class NavBarModule {}
