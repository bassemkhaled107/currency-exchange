import { FormControl } from "@angular/forms";
import { CustomValidators } from "./custom-validators";

describe("CustomValidators", () => {
  describe("required", () => {
    it("should return required if field is empty", () => {
      const field = new FormControl("");
      const result = CustomValidators.required(field);

      expect(result).toStrictEqual({ required: true });
    });

    it("should pass validation if field has a valid value", () => {
      const field = new FormControl(" mohamed ");
      const result = CustomValidators.required(field);

      expect(result).toBeNull();
    });
  });

  describe("minLength", () => {
    it("should return minlength if field has min that length", () => {
      const field = new FormControl("12");
      const result = CustomValidators.minLength(3)(field);

      expect(result).toStrictEqual({ minlength: true });
    });

    it("should pass validation if field has a valid min length", () => {
      const field = new FormControl(" mohamed ");
      const result = CustomValidators.minLength(6)(field);

      expect(result).toBeNull();
    });
  });

  describe("maxLength", () => {
    it("should return maxlength if field has max that length", () => {
      const field = new FormControl("123456789");
      const result = CustomValidators.maxLength(6)(field);

      expect(result).toStrictEqual({ maxlength: true });
    });

    it("should pass validation if field has a valid max length", () => {
      const field = new FormControl(" mohamed ");
      const result = CustomValidators.maxLength(9)(field);

      expect(result).toBeNull();
    });
  });

  describe("minLengthArray", () => {
    it("should return minLengthArray if field has min that length", () => {
      const field = new FormControl(["1", "2"]);
      const result = CustomValidators.minLengthArray(3)(field);

      expect(result).toStrictEqual({
        minLengthArray: { valid: false, invalid: true },
      });
    });

    it("should pass validation if field has a valid min length", () => {
      const field = new FormControl(["1", "2", "3", "4", "5", "6"]);
      const result = CustomValidators.minLengthArray(6)(field);

      expect(result).toBeNull();
    });
  });

  describe("maxLengthArray", () => {
    it("should return maxLengthArray if field has max that length", () => {
      const field = new FormControl(["1", "2", "3", "4"]);
      const result = CustomValidators.maxLengthArray(3)(field);

      expect(result).toStrictEqual({
        maxLengthArray: { valid: false, invalid: true },
      });
    });

    it("should pass validation if field has a valid max length", () => {
      const field = new FormControl(["1", "2", "3"]);
      const result = CustomValidators.maxLengthArray(6)(field);

      expect(result).toBeNull();
    });
  });

  describe("lengthArray", () => {
    it("should return lengthArray if field has equal that length", () => {
      const field = new FormControl(["1", "2", "3", "4"]);
      const result = CustomValidators.lengthArray(3)(field);

      expect(result).toStrictEqual({
        lengthArray: { valid: false, invalid: true },
      });
    });

    it("should pass validation if field has a valid equal length", () => {
      const field = new FormControl(["1", "2", "3"]);
      const result = CustomValidators.lengthArray(3)(field);

      expect(result).toBeNull();
    });
  });

  describe("existsInArr", () => {
    it("should return existsInArr if field has this value", () => {
      const field = new FormControl("1");
      const result = CustomValidators.existsInArr(["1", "2", "3", "4"])(field);

      expect(result).toStrictEqual({ existsInArr: true });
    });

    it("should pass validation if field has not this value", () => {
      const field = new FormControl("1");
      const result = CustomValidators.existsInArr(["4", "5"])(field);

      expect(result).toBeNull();
    });
  });

  describe("phoneValidator", () => {
    it("should return phoneValidator if number is not phone number", () => {
      const field = new FormControl("0109123");
      field.markAsDirty();
      const result = CustomValidators.phoneValidator(field);

      expect(result).toStrictEqual({ phoneValidator: true });
    });

    it("should pass validation if number is a valid mobile number", () => {
      const field = new FormControl("0101234567");
      field.markAsDirty();
      const result = CustomValidators.phoneValidator(field);

      expect(result).toBeNull();
    });
  });

  describe("urlValidator", () => {
    it("should return urlValidator if filed is not url link", () => {
      const field = new FormControl("gom");
      field.markAsDirty();
      const result = CustomValidators.urlValidator(field);

      expect(result).toStrictEqual({ urlValidator: true });
    });

    it("should pass validation if filed is a valid url link", () => {
      const field = new FormControl("https://google.com");
      field.markAsDirty();
      const result = CustomValidators.urlValidator(field);

      expect(result).toBeNull();
    });
  });

  describe("isZero", () => {
    it("should return isZero if filed is not zero", () => {
      const field = new FormControl(0);
      const result = CustomValidators.isZero(field);

      expect(result).toStrictEqual({ zero: true });
    });

    it("should pass validation if filed equals zero number", () => {
      const field = new FormControl("0");
      const result = CustomValidators.isZero(field);

      expect(result).toBeNull();
    });

    it("should pass validation if filed equals zero", () => {
      const field = new FormControl(5);
      const result = CustomValidators.isZero(field);

      expect(result).toBeNull();
    });
  });
});
