import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import * as fromRouter from "@ngrx/router-store";

import * as fromSharedModels from "../shared-models";

// Custom Serializer for ngrx router store
@Injectable()
export class CustomSerializer
  implements fromRouter.RouterStateSerializer<fromSharedModels.RouterStateUrl>
{
  serialize(routerState: RouterStateSnapshot): fromSharedModels.RouterStateUrl {
    const { url } = routerState;
    const { queryParams } = routerState.root;

    let state: ActivatedRouteSnapshot = routerState.root;
    while (state.firstChild) {
      state = state.firstChild;
    }
    const { params } = state;
    return { url, queryParams, params };
  }
}
