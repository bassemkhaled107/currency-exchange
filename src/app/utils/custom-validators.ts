import {
  AbstractControl,
  FormArray,
  FormControl,
  ValidatorFn,
} from "@angular/forms";

export class CustomValidators {
  //validation to check if the number is smaller than zero or equal zero
  public static isNegativeOrZero(control: FormControl): { positive: boolean } {
    return +control.value <= 0 ? { positive: true } : null;
  }
}
