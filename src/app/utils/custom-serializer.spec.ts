import { CustomSerializer } from "./custom-serializer";

describe("CustomSerializer", () => {
  it("should return url", () => {
    const routerState = {
      url: "/style",
      root: {
        queryParams: {},
        firstChild: {
          params: {},
        },
      },
    };

    const instance = new CustomSerializer();
    const result = instance.serialize(routerState as any);

    expect(instance).toBeInstanceOf(CustomSerializer);
    expect(result).toStrictEqual({
      url: "/style",
      queryParams: {},
      params: {},
    });
  });

  it("should return query params", () => {
    const routerState = {
      url: "/auth/login",
      root: {
        queryParams: {
          lang: "en",
        },
        firstChild: {
          params: {},
        },
      },
    };

    const instance = new CustomSerializer();
    const result = instance.serialize(routerState as any);

    expect(instance).toBeInstanceOf(CustomSerializer);
    expect(result).toStrictEqual({
      url: "/auth/login",
      queryParams: {
        lang: "en",
      },
      params: {},
    });
  });
});
