export interface ICurrencyExchangeResponse {
  data: {
    [key: string]: number;
  };
}
