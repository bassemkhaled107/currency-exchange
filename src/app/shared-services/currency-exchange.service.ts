import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

import { environment } from "src/environments/environment";
import * as fromSharedModels from "../shared-models";

@Injectable()
export class CurrencyExchangeService {
  constructor(private http: HttpClient) {}

  // convertCurrency(base_currency: string, currencies: string[]) {
  //   return this.http.post<void>(
  //     `${environment.apiUrl}/latest?apiKey=${
  //       environment.apiKey
  //     }&base_currency=${base_currency}&currencies=${currencies.join(',')}`,
  //     {}
  //   );
  // }

  currencyExchange(
    baseCurrency: string,
    convertToCurrency: string
  ): Observable<fromSharedModels.ICurrencyExchangeResponse> {
    return this.http.get<fromSharedModels.ICurrencyExchangeResponse>(
      `${environment.apiUrl}/latest?apikey=${environment.apiKey}&base_currency=${baseCurrency}&currencies=${convertToCurrency}`,
      {}
    );
  }
}
