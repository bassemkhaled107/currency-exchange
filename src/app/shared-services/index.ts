import { CurrenciesService } from "./currencies.service";
import { CurrencyExchangeService } from "./currency-exchange.service";

export const SERVICES = [CurrencyExchangeService, CurrenciesService];

export { CurrencyExchangeService } from "./currency-exchange.service";
export { CurrenciesService } from "./currencies.service";
