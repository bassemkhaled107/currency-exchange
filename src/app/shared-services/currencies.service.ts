import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

import { environment } from "src/environments/environment";
import * as fromSharedModels from "../shared-models";

@Injectable()
export class CurrenciesService {
  constructor(private http: HttpClient) {}

  loadCurrencies(): Observable<fromSharedModels.ICurrenciesResponse> {
    return this.http.get<fromSharedModels.ICurrenciesResponse>(
      `${environment.apiUrl}/currencies?apikey=${environment.apiKey}`
    );
  }
}
