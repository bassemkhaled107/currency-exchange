import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { ReactiveFormsModule } from "@angular/forms";

import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";

import { CurrencyDetailsRoutingModule } from "./currency-details-routing.module";
import { EffectsModule } from "@ngrx/effects";
import { StoreModule } from "@ngrx/store";

import * as fromContainers from "./containers/";
import * as fromStore from "./store";
import * as fromServices from "./services";

import { CurrencyExchangerPanelModule } from "@shared/currency-exchanger-panel/currency-exchanger-panel.module";
import { NavBarModule } from "@shared/nav-bar/nav-bar.module";

import { NgxChartsModule } from "@swimlane/ngx-charts";

const MATERIALS = [MatSnackBarModule, MatProgressSpinnerModule];

@NgModule({
  declarations: [...fromContainers.CONTAINERS],
  imports: [
    CommonModule,
    CurrencyDetailsRoutingModule,
    ReactiveFormsModule,
    CurrencyExchangerPanelModule,
    NavBarModule,
    ...MATERIALS,
    NgxChartsModule,
    StoreModule.forFeature("currencyDetailsFeature", fromStore.reducers),
    EffectsModule.forFeature(fromStore.EFFECTS),
  ],
  providers: [...fromServices.SERVICES],
})
export class CurrencyDetailsModule {}
