import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

import { environment } from "src/environments/environment";
import * as fromModels from "../models";

@Injectable()
export class CurrencyService {
  constructor(private http: HttpClient) {}

  loadCurrency(currency: string): Observable<fromModels.ICurrencyResponse> {
    return this.http.get<fromModels.ICurrencyResponse>(
      `${environment.apiUrl}/currencies?apikey=${environment.apiKey}&currencies=${currency}`
    );
  }

  loadCurrencyHistoricalRates(
    baseCurrency: string,
    convertedCurrency: string,
    dateFrom: string,
    dateTo: string
  ): Observable<fromModels.ICurrencyHistoricalRates> {
    return this.http.get<fromModels.ICurrencyHistoricalRates>(
      `${environment.apiUrl}/historical?apikey=${environment.apiKey}&base_currency=${baseCurrency}&currencies=${convertedCurrency}&date_from=${dateFrom}&date_to=${dateTo}`,
      {}
    );
  }
}
