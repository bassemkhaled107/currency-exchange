export interface ICurrencyHistoricalRates {
  data: {
    [key: string]: {
      [key: string]: number;
    };
  };
}
