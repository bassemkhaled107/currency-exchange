import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { MatSnackBar } from "@angular/material/snack-bar";

import { Actions, createEffect, ofType } from "@ngrx/effects";
import { map, catchError, tap, switchMap } from "rxjs/operators";
import { of } from "rxjs";
import * as fromServices from "../../services";
import * as fromActions from "../actions";

@Injectable()
export class CurrencyEffects {
  constructor(
    private actions$: Actions,
    private currencyService: fromServices.CurrencyService,
    private snackBar: MatSnackBar,
    private router: Router
  ) {}

  loadCurrency$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.loadCurrency),
      switchMap(({ payload }) =>
        this.currencyService.loadCurrency(payload.currency).pipe(
          map((data) => fromActions.loadCurrencySuccess({ payload: data })),
          catchError((error) =>
            of(
              fromActions.loadCurrencyFailure({
                payload: error && error.error,
              })
            )
          )
        )
      )
    )
  );

  loadCurrencyFailure$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(fromActions.loadCurrencyFailure),
        tap(({ payload }) => {
          this.snackBar.open("Something went wrong", "", {
            duration: 3000,
          });
          this.router.navigate(["/home"]);
        })
      );
    },
    { dispatch: false }
  );

  loadCurrencyHistoricalRates$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.loadCurrencyHistoricalRates),
      switchMap(({ payload }) =>
        this.currencyService
          .loadCurrencyHistoricalRates(
            payload.baseCurrency,
            payload.convertedCurrency,
            payload.dateFrom,
            payload.dateTo
          )
          .pipe(
            map((data) =>
              fromActions.loadCurrencyHistoricalRatesSuccess({ payload: data })
            ),
            catchError((error) =>
              of(
                fromActions.loadCurrencyHistoricalRatesFailure({
                  payload: error && error.error,
                })
              )
            )
          )
      )
    )
  );

  loadCurrencyHistoricalRatesFailure$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(fromActions.loadCurrencyHistoricalRatesFailure),
        tap(({ payload }) => {
          this.snackBar.open("Something went wrong", "", {
            duration: 3000,
          });
          this.router.navigate(["/home"]);
        })
      );
    },
    { dispatch: false }
  );
}
