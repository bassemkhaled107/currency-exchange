import { createAction, props } from "@ngrx/store";
import * as fromModels from "../../models";

export const loadCurrency = createAction(
  "[Currency Details] load currency",

  props<{ payload: { currency: string } }>()
);
export const loadCurrencySuccess = createAction(
  "[Currency Details] load currency success",
  props<{ payload: fromModels.ICurrencyResponse }>()
);

export const loadCurrencyFailure = createAction(
  "[Currency Details] load currency failure",
  props<{ payload: Error }>()
);

export const loadCurrencyHistoricalRates = createAction(
  "[Currency Details] load currency historical rates",

  props<{
    payload: {
      baseCurrency: string;
      convertedCurrency: string;
      dateFrom: string;
      dateTo: string;
    };
  }>()
);
export const loadCurrencyHistoricalRatesSuccess = createAction(
  "[Currency Details] load currency historical rates success",
  props<{ payload: fromModels.ICurrencyHistoricalRates }>()
);

export const loadCurrencyHistoricalRatesFailure = createAction(
  "[Currency Details] load currency historical rates failure",
  props<{ payload: Error }>()
);
