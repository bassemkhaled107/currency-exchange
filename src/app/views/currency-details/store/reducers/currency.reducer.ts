import { Action, createReducer, on } from "@ngrx/store";
import * as fromSharedActions from "../actions";
import * as fromModels from "../../models";

export interface ICurrencyState {
  currencyLoading: boolean;
  currencyError: Error;
  currencyResponse: fromModels.ICurrencyResponse;

  currencyHistoricalRatesPending: boolean;
  currencyHistoricalRatesError: Error;
  currencyHistoricalRatesResponse: fromModels.ICurrencyHistoricalRates;
}

export const initialState: ICurrencyState = {
  currencyLoading: false,
  currencyError: undefined,
  currencyResponse: undefined,

  currencyHistoricalRatesPending: false,
  currencyHistoricalRatesError: undefined,
  currencyHistoricalRatesResponse: undefined,
};

export const CurrencyReducer = createReducer(
  initialState,

  on(fromSharedActions.loadCurrency, (state) => ({
    ...state,
    currencyLoading: true,
    currencyError: undefined,
  })),

  on(fromSharedActions.loadCurrencySuccess, (state, { payload }) => ({
    ...state,
    currencyLoading: false,
    currencyError: undefined,
    currencyResponse: payload,
  })),

  on(fromSharedActions.loadCurrencyFailure, (state, { payload }) => ({
    ...state,
    currencyLoading: false,
    currencyError: payload,
  })),
  on(fromSharedActions.loadCurrencyHistoricalRates, (state) => ({
    ...state,
    currencyHistoricalRatesPending: true,
    currencyHistoricalRatesError: undefined,
  })),

  on(
    fromSharedActions.loadCurrencyHistoricalRatesSuccess,
    (state, { payload }) => ({
      ...state,
      currencyHistoricalRatesPending: false,
      currencyHistoricalRatesError: undefined,
      currencyHistoricalRatesResponse: payload,
    })
  ),

  on(
    fromSharedActions.loadCurrencyHistoricalRatesFailure,
    (state, { payload }) => ({
      ...state,
      currencyHistoricalRatesPending: false,
      currencyHistoricalRatesError: payload,
    })
  )
);

export function reducer(
  state: ICurrencyState | undefined,
  action: Action
): ICurrencyState {
  return CurrencyReducer(state, action);
}

export const getCurrencyDetailsCurrencyLoading = (state: ICurrencyState) =>
  state.currencyLoading;
export const getCurrencyDetailsCurrencyError = (state: ICurrencyState) =>
  state.currencyError;
export const getCurrencyDetailsCurrencyResponse = (state: ICurrencyState) =>
  state.currencyResponse;

export const getCurrencyDetailsCurrencyHistoricalRatesPending = (
  state: ICurrencyState
) => state.currencyHistoricalRatesPending;
export const getCurrencyDetailsCurrencyHistoricalRatesError = (
  state: ICurrencyState
) => state.currencyHistoricalRatesError;
export const getCurrencyDetailsCurrencyHistoricalRatesResponse = (
  state: ICurrencyState
) => state.currencyHistoricalRatesResponse;
