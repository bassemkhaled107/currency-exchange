import { ActionReducerMap, createFeatureSelector } from "@ngrx/store";

import * as fromCurrencyReducer from "./currency.reducer";

export interface IFeatureState {
  currency: fromCurrencyReducer.ICurrencyState;
}

export const reducers: ActionReducerMap<IFeatureState> = {
  currency: fromCurrencyReducer.reducer,
};

export const getIFeatureState = createFeatureSelector<IFeatureState>(
  "currencyDetailsFeature"
);
