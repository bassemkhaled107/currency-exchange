import * as fromSharedStore from "src/app/shared-store";

export const getQueryParamsByRouter = fromSharedStore.getQueryParamsByRouter;

export const getCurrenciesLoading = fromSharedStore.getSharedCurrenciesLoading;
export const getCurrencyExchangePending =
  fromSharedStore.getSharedCurrencyExchangePending;
