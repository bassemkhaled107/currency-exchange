import { createSelector } from "@ngrx/store";

import * as fromFeature from "../reducers";
import * as fromCurrencyReducer from "../reducers/currency.reducer";

export const getCurrencyState = createSelector(
  fromFeature.getIFeatureState,
  (state: fromFeature.IFeatureState) => state.currency
);

export const getCurrencyDetailsCurrencyLoading = createSelector(
  getCurrencyState,
  fromCurrencyReducer.getCurrencyDetailsCurrencyLoading
);

export const getCurrencyDetailsCurrencyError = createSelector(
  getCurrencyState,
  fromCurrencyReducer.getCurrencyDetailsCurrencyError
);
export const getCurrencyDetailsCurrencyResponse = createSelector(
  getCurrencyState,
  fromCurrencyReducer.getCurrencyDetailsCurrencyResponse
);

// format the currency json to be a simple object
export const getCurrencyDetails = createSelector(
  getCurrencyDetailsCurrencyResponse,

  (currencies) => {
    if (currencies) {
      const dynamicKey = Object.keys(currencies?.data)[0]; // the currency returned from the api
      const currencyObject = currencies?.data[dynamicKey];

      return currencyObject;
    }
    return null;
  }
);

export const getCurrencyDetailsCurrencyHistoricalRatesPending = createSelector(
  getCurrencyState,
  fromCurrencyReducer.getCurrencyDetailsCurrencyHistoricalRatesPending
);

export const getCurrencyDetailsCurrencyHistoricalRatesError = createSelector(
  getCurrencyState,
  fromCurrencyReducer.getCurrencyDetailsCurrencyHistoricalRatesError
);
export const getCurrencyDetailsCurrencyHistoricalRatesResponse = createSelector(
  getCurrencyState,
  fromCurrencyReducer.getCurrencyDetailsCurrencyHistoricalRatesResponse
);

// format the historical rates to make the month rate  is the rate of last day in the month
// and return the data in array of objects
export const getCurrencyHistoricalRates = createSelector(
  getCurrencyDetailsCurrencyHistoricalRatesResponse,

  (rates) => {
    if (rates?.data) {
      const lastDayValues = {};
      let prevMonth = null;
      let prevYear = null;

      for (const date in rates.data) {
        const [year, month] = date.split("-");
        const currentMonth = parseInt(month);
        const currentYear = parseInt(year);

        if (prevMonth === null) {
          prevMonth = currentMonth;
          prevYear = currentYear;
        }

        if (currentMonth !== prevMonth || currentYear !== prevYear) {
          const lastDay = `${prevYear}-${prevMonth
            .toString()
            .padStart(2, "0")}-${new Date(prevYear, prevMonth, 0).getDate()}`;
          lastDayValues[lastDay] = rates.data[lastDay];
        }

        prevMonth = currentMonth;
        prevYear = currentYear;
      }

      const currencyHistoricalRates = Object.entries(lastDayValues).map(
        ([name, rate]) => ({
          name,
          value: rate[Object.keys(rate)[0]],
        })
      );

      return currencyHistoricalRates;
    }
    return null;
  }
);
