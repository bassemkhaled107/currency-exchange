import { Component, OnInit } from "@angular/core";
import { Store, select } from "@ngrx/store";
import { Observable } from "rxjs";

import * as fromStore from "../../store";
import * as fromActions from "../../store/actions";
import * as fromModels from "../../models";

@Component({
  selector: "app-currency-details",
  templateUrl: "./currency-details.component.html",
  styleUrls: ["./currency-details.component.scss"],
})
export class CurrencyDetailsComponent implements OnInit {
  currency$: Observable<fromModels.ICurrency>;
  currencyLoading$: Observable<boolean>;
  currenciesLoading$: Observable<boolean>;
  currencyExchangePending$: Observable<boolean>;
  currencyHistoricalRatesPending$: Observable<boolean>;

  currencyHistoricalRates$: Observable<{ name: string; value: number }[]>; // change the model because the chart take the data in array of object that contains name and value as a keys

  colorScheme = {
    domain: ["#871e35", "#e8ba00"],
  }; // color scheme of charts

  queryParams: {
    amount: string;
    from: string;
    to: string;
  };
  constructor(private store: Store<fromStore.IFeatureState>) {}

  ngOnInit(): void {
    this.store
      .pipe(select(fromStore.getQueryParamsByRouter))
      .subscribe((queryParams) => {
        if (queryParams.from) {
          this.queryParams = queryParams;
          this.store.dispatch(
            fromActions.loadCurrency({
              payload: {
                currency: queryParams.from,
              },
            })
          );
        }
      });

    this.currency$ = this.store.pipe(select(fromStore.getCurrencyDetails));
    this.currencyHistoricalRates$ = this.store.pipe(
      select(fromStore.getCurrencyHistoricalRates)
    );

    this.currenciesLoading$ = this.store.pipe(
      select(fromStore.getCurrenciesLoading)
    );
    this.currencyExchangePending$ = this.store.pipe(
      select(fromStore.getCurrencyExchangePending)
    );

    this.currencyLoading$ = this.store.pipe(
      select(fromStore.getCurrencyDetailsCurrencyLoading)
    );

    this.currencyHistoricalRatesPending$ = this.store.pipe(
      select(fromStore.getCurrencyDetailsCurrencyHistoricalRatesPending)
    );

    this.store
      .pipe(select(fromStore.getCurrencyHistoricalRates))
      .subscribe((data) => {});
  }

  // dispatch to load currency historical rates for last year
  //when user click on convert currency from currency exchange panel module
  onLoadCurrencyHistoricalRates(args: {
    baseCurrency: string;
    convertedToCurrency: string;
    amount: number;
  }) {
    const pastYear = new Date().getFullYear() - 1;
    // Get the current date
    const currentDate = new Date();

    // Format the last year's date as desired (e.g., yyyy-mm-dd)
    const formattedDateFrom = new Date(
      currentDate.getFullYear() - 1,
      1,
      1
    ).toISOString();

    const formattedDateTo = new Date(
      currentDate.getFullYear() - 1,
      12,
      31
    ).toISOString();

    this.store.dispatch(
      fromActions.loadCurrencyHistoricalRates({
        payload: {
          baseCurrency: args.baseCurrency,
          convertedCurrency: args.convertedToCurrency,
          dateFrom: formattedDateFrom,
          dateTo: formattedDateTo,
        },
      })
    );
  }
}
