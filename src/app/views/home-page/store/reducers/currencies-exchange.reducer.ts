import { Action, createReducer, on } from "@ngrx/store";
import * as fromSharedActions from "../actions";
import * as fromModels from "../../models";

export interface ICurrenciesExchangeState {
  currenciesExchangePending: boolean;
  currenciesExchangeError: Error;
  currenciesExchangeResult: fromModels.ICurrenciesExchangeResponse;
}

export const initialState: ICurrenciesExchangeState = {
  currenciesExchangePending: false,
  currenciesExchangeError: undefined,
  currenciesExchangeResult: undefined,
};

export const CurrenciesExchangeReducer = createReducer(
  initialState,

  on(fromSharedActions.currenciesExchange, (state) => ({
    ...state,
    currenciesExchangePending: true,
    currenciesExchangeError: undefined,
  })),

  on(fromSharedActions.currenciesExchangeSuccess, (state, { payload }) => ({
    ...state,
    currenciesExchangePending: false,
    currenciesExchangeError: undefined,
    currenciesExchangeResult: payload,
  })),

  on(fromSharedActions.currenciesExchangeFailure, (state, { payload }) => ({
    ...state,
    currenciesExchangePending: false,
    currenciesExchangeError: payload,
  }))
);

export function reducer(
  state: ICurrenciesExchangeState | undefined,
  action: Action
): ICurrenciesExchangeState {
  return CurrenciesExchangeReducer(state, action);
}

export const getHomePageCurrenciesExchangePending = (
  state: ICurrenciesExchangeState
) => state.currenciesExchangePending;
export const getHomePageCurrenciesExchangeError = (
  state: ICurrenciesExchangeState
) => state.currenciesExchangeError;
export const getHomePageCurrenciesExchangeResult = (
  state: ICurrenciesExchangeState
) => state.currenciesExchangeResult;
