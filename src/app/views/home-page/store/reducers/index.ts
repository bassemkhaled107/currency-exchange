import { ActionReducerMap, createFeatureSelector } from "@ngrx/store";

import * as fromCurrenciesExchangeReducer from "./currencies-exchange.reducer";

export interface IFeatureState {
  currenciesExchange: fromCurrenciesExchangeReducer.ICurrenciesExchangeState;
}

export const reducers: ActionReducerMap<IFeatureState> = {
  currenciesExchange: fromCurrenciesExchangeReducer.reducer,
};

export const getIFeatureState = createFeatureSelector<IFeatureState>(
  "currenciesExchangeFeature"
);
