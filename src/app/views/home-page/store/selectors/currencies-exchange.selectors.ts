import { createSelector } from "@ngrx/store";

import * as fromFeature from "../reducers";
import * as fromCurrencyExchangeReducer from "../reducers/currencies-exchange.reducer";

export const getCurrenciesExchangeState = createSelector(
  fromFeature.getIFeatureState,
  (state: fromFeature.IFeatureState) => state.currenciesExchange
);

export const getHomePageCurrenciesExchangePending = createSelector(
  getCurrenciesExchangeState,
  fromCurrencyExchangeReducer.getHomePageCurrenciesExchangePending
);

export const getHomePageCurrenciesExchangeError = createSelector(
  getCurrenciesExchangeState,
  fromCurrencyExchangeReducer.getHomePageCurrenciesExchangeError
);
export const getHomePageCurrenciesExchangeResult = createSelector(
  getCurrenciesExchangeState,
  fromCurrencyExchangeReducer.getHomePageCurrenciesExchangeResult
);

// convert object of objects of the converted currencies to array of objects to loop on them in html
export const getCurrenciesExchangeInArray = createSelector(
  getHomePageCurrenciesExchangeResult,

  (currencies) => {
    return currencies
      ? Object.entries(currencies?.data).map(([currencyName, rate]) => ({
          currencyName,
          rate,
        }))
      : null;
  }
);
