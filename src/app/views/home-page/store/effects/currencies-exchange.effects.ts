import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { MatSnackBar } from "@angular/material/snack-bar";

import { Actions, createEffect, ofType } from "@ngrx/effects";
import { map, catchError, tap, switchMap } from "rxjs/operators";
import { of } from "rxjs";
import * as fromServices from "../../services";
import * as fromActions from "../actions";

@Injectable()
export class CurrenciesExchangeEffects {
  constructor(
    private actions$: Actions,
    private currenciesExchangeService: fromServices.CurrenciesExchangeService,
    private snackBar: MatSnackBar,
    private router: Router
  ) {}

  currenciesExchange$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.currenciesExchange),
      switchMap(({ payload }) =>
        this.currenciesExchangeService
          .currenciesExchange(payload.baseCurrency, payload.currencies)
          .pipe(
            map((data) =>
              fromActions.currenciesExchangeSuccess({ payload: data })
            ),
            catchError((error) =>
              of(
                fromActions.currenciesExchangeFailure({
                  payload: error && error.error,
                })
              )
            )
          )
      )
    )
  );

  currenciesExchangeFailure$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(fromActions.currenciesExchangeFailure),
        tap(({ payload }) => {
          this.snackBar.open("Something went wrong", "", {
            duration: 3000,
          });
          this.router.navigate(["/home"]);
        })
      );
    },
    { dispatch: false }
  );
}
