import { CurrenciesExchangeEffects } from "./currencies-exchange.effects";

export const EFFECTS: any = [CurrenciesExchangeEffects];

export { CurrenciesExchangeEffects } from "./currencies-exchange.effects";
