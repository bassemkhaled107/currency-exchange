import { createAction, props } from "@ngrx/store";
import * as fromModels from "../../models";

export const currenciesExchange = createAction(
  "[Home Page] convert currencies",
  props<{
    payload: {
      baseCurrency: string;
      currencies: string[];
    };
  }>()
);
export const currenciesExchangeSuccess = createAction(
  "[Home Page] convert currencies success",
  props<{ payload: fromModels.ICurrenciesExchangeResponse }>()
);

export const currenciesExchangeFailure = createAction(
  "[Home Page] convert currencies failure",
  props<{ payload: Error }>()
);
