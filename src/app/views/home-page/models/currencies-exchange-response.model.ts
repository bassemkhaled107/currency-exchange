export interface ICurrenciesExchangeResponse {
  data: {
    [key: string]: number;
  };
}
