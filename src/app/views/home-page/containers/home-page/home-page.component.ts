import { Component, OnInit } from "@angular/core";
import { Store, select } from "@ngrx/store";
import { Observable } from "rxjs";

import * as fromStore from "../../store";
import * as fromActions from "../../store/actions";
@Component({
  selector: "app-home-page",
  templateUrl: "./home-page.component.html",
  styleUrls: ["./home-page.component.scss"],
})
export class HomePageComponent implements OnInit {
  currencies$: Observable<{ currencyName: string; rate: number }[]>;
  currenciesExchangePending$: Observable<boolean>;

  currenciesLoading$: Observable<boolean>;
  currencyExchangePending$: Observable<boolean>;

  amount: number; // amount number to convert

  popularCurrencies: string[] = [
    "EUR",
    "USD",
    "JPY",
    "GBP",
    "CHF",
    "TRY",
    "CAD",
    "CNY",
    "INR",
    "RUB",
  ];

  constructor(private store: Store<fromStore.IFeatureState>) {}
  ngOnInit(): void {
    this.currencies$ = this.store.pipe(
      select(fromStore.getCurrenciesExchangeInArray)
    );
    this.currenciesExchangePending$ = this.store.pipe(
      select(fromStore.getHomePageCurrenciesExchangePending)
    );
    this.currenciesLoading$ = this.store.pipe(
      select(fromStore.getCurrenciesLoading)
    );
    this.currencyExchangePending$ = this.store.pipe(
      select(fromStore.getCurrencyExchangePending)
    );
  }

  // dispatch to load currency converted to 9 different currencies
  //when user click on convert currency at currency exchange panel
  onCurrenciesExchange(args: {
    baseCurrency: string;
    convertedToCurrency: string;
    amount: number;
  }) {
    this.amount = args.amount;
    this.store.dispatch(
      fromActions.currenciesExchange({
        payload: {
          baseCurrency: args.baseCurrency,
          currencies: this.popularCurrencies.filter(
            (currency) => currency !== args.convertedToCurrency
          ),
        },
      })
    );
  }
}
