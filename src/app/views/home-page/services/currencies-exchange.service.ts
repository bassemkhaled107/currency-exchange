import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

import { environment } from "src/environments/environment";
import * as fromModels from "../models";

@Injectable()
export class CurrenciesExchangeService {
  constructor(private http: HttpClient) {}

  currenciesExchange(
    baseCurrency: string,
    currencies: string[]
  ): Observable<fromModels.ICurrenciesExchangeResponse> {
    return this.http.get<fromModels.ICurrenciesExchangeResponse>(
      `${environment.apiUrl}/latest?apikey=${
        environment.apiKey
      }&base_currency=${baseCurrency}&currencies=${currencies.join(",")}`,
      {}
    );
  }
}
