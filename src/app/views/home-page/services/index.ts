import { CurrenciesExchangeService } from "./currencies-exchange.service";

export const SERVICES = [CurrenciesExchangeService];

export { CurrenciesExchangeService } from "./currencies-exchange.service";
