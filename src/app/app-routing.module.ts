import { NgModule } from "@angular/core";
import { ExtraOptions, RouterModule, Routes } from "@angular/router";

export const routingConfiguration: ExtraOptions = {
  paramsInheritanceStrategy: "always",
};

const routes: Routes = [
  {
    path: "home",
    loadChildren: () =>
      import("./views/home-page/home-page.module").then(
        (module) => module.HomePageModule
      ),
  },

  {
    path: "currency-details",
    loadChildren: () =>
      import("./views/currency-details/currency-details.module").then(
        (module) => module.CurrencyDetailsModule
      ),
  },
  { path: "", redirectTo: "home", pathMatch: "full" },
  {
    path: "**",
    loadChildren: () =>
      import("./views/not-found/not-found.module").then(
        (module) => module.NotFoundModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, routingConfiguration)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
