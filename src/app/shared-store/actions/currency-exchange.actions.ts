import { createAction, props } from "@ngrx/store";
import * as fromSharedModels from "../../shared-models";

export const currencyExchange = createAction(
  "[Shared] convert currency",
  props<{
    payload: {
      baseCurrency: string;
      convertToCurrency: string;
    };
  }>()
);
export const currencyExchangeSuccess = createAction(
  "[Shared] convert currency success",
  props<{ payload: fromSharedModels.ICurrencyExchangeResponse }>()
);

export const currencyExchangeFailure = createAction(
  "[Shared] convert currency failure",
  props<{ payload: Error }>()
);
