import { createAction, props } from "@ngrx/store";
import * as fromSharedModels from "../../shared-models";

export const loadCurrencies = createAction("[Shared] load currencies");
export const loadCurrenciesSuccess = createAction(
  "[Shared] load currencies success",
  props<{ payload: fromSharedModels.ICurrenciesResponse }>()
);

export const loadCurrenciesFailure = createAction(
  "[Shared] load currencies failure",
  props<{ payload: Error }>()
);
