import { createSelector } from "@ngrx/store";

import * as fromRoot from "../reducers";
import * as fromCurrenciesReducer from "../reducers/currencies.reducer";

export const getSharedCurrenciesLoading = createSelector(
  fromRoot.getCurrenciesState,
  fromCurrenciesReducer.getSharedCurrenciesLoading
);

export const getSharedCurrenciesError = createSelector(
  fromRoot.getCurrenciesState,
  fromCurrenciesReducer.getSharedCurrenciesError
);

export const getSharedCurrenciesResponse = createSelector(
  fromRoot.getCurrenciesState,
  fromCurrenciesReducer.getSharedCurrenciesResponse
);

// convert object of objects to array of objects to loop on them in html
export const getCurrenciesInArray = createSelector(
  getSharedCurrenciesResponse,

  (currencies) => {
    return currencies ? Object.values(currencies?.data) : null;
  }
);
