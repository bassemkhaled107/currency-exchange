import { createSelector } from "@ngrx/store";

import * as fromSharedStore from "src/app/shared-store";

export const getQueryParamsByRouter = createSelector(
  fromSharedStore.getRouterState,
  (router: any) => router && router.state?.queryParams
);
