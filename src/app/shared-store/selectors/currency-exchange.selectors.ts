import { createSelector } from "@ngrx/store";

import * as fromRoot from "../reducers";
import * as fromCurrencyExchangeReducer from "../reducers/currency-exchange.reducer";

export const getSharedCurrencyExchangePending = createSelector(
  fromRoot.getCurrencyExchangeState,
  fromCurrencyExchangeReducer.getSharedCurrencyExchangePending
);

export const getSharedCurrencyExchangeError = createSelector(
  fromRoot.getCurrencyExchangeState,
  fromCurrencyExchangeReducer.getSharedCurrencyExchangeError
);

export const getSharedCurrencyExchangeResult = createSelector(
  fromRoot.getCurrencyExchangeState,
  fromCurrencyExchangeReducer.getSharedCurrencyExchangeResult
);
