import { Action, createReducer, on } from "@ngrx/store";
import * as fromSharedActions from "../actions";
import * as fromSharedModels from "../../shared-models";

export interface ICurrenciesState {
  currenciesLoading: boolean;
  currenciesError: Error;
  currenciesResponse: fromSharedModels.ICurrenciesResponse;
}

export const initialState: ICurrenciesState = {
  currenciesLoading: false,
  currenciesError: undefined,
  currenciesResponse: undefined,
};

export const CurrenciesReducer = createReducer(
  initialState,

  on(fromSharedActions.loadCurrencies, (state) => ({
    ...state,
    currenciesLoading: true,
    currenciesError: undefined,
  })),

  on(fromSharedActions.loadCurrenciesSuccess, (state, { payload }) => ({
    ...state,
    currenciesLoading: false,
    currenciesError: undefined,
    currenciesResponse: payload,
  })),

  on(fromSharedActions.loadCurrenciesFailure, (state, { payload }) => ({
    ...state,
    currenciesLoading: false,
    currenciesError: payload,
  }))
);

export function reducer(
  state: ICurrenciesState | undefined,
  action: Action
): ICurrenciesState {
  return CurrenciesReducer(state, action);
}

export const getSharedCurrenciesLoading = (state: ICurrenciesState) =>
  state.currenciesLoading;
export const getSharedCurrenciesError = (state: ICurrenciesState) =>
  state.currenciesError;
export const getSharedCurrenciesResponse = (state: ICurrenciesState) =>
  state.currenciesResponse;
