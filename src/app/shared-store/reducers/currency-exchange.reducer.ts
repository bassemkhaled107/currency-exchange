import { Action, createReducer, on } from "@ngrx/store";
import * as fromSharedActions from "../actions";
import * as fromSharedModels from "../../shared-models";

export interface ICurrencyExchangeState {
  currencyExchangePending: boolean;
  currencyExchangeError: Error;
  currencyExchangeResult: fromSharedModels.ICurrencyExchangeResponse;
}

export const initialState: ICurrencyExchangeState = {
  currencyExchangePending: false,
  currencyExchangeError: undefined,
  currencyExchangeResult: undefined,
};

export const CurrencyExchangeReducer = createReducer(
  initialState,

  on(fromSharedActions.currencyExchange, (state) => ({
    ...state,
    currencyExchangePending: true,
    currencyExchangeError: undefined,
  })),

  on(fromSharedActions.currencyExchangeSuccess, (state, { payload }) => ({
    ...state,
    currencyExchangePending: false,
    currencyExchangeError: undefined,
    currencyExchangeResult: payload,
  })),

  on(fromSharedActions.currencyExchangeFailure, (state, { payload }) => ({
    ...state,
    currencyExchangePending: false,
    currencyExchangeError: payload,
  }))
);

export function reducer(
  state: ICurrencyExchangeState | undefined,
  action: Action
): ICurrencyExchangeState {
  return CurrencyExchangeReducer(state, action);
}

export const getSharedCurrencyExchangePending = (
  state: ICurrencyExchangeState
) => state.currencyExchangePending;
export const getSharedCurrencyExchangeError = (state: ICurrencyExchangeState) =>
  state.currencyExchangeError;
export const getSharedCurrencyExchangeResult = (
  state: ICurrencyExchangeState
) => state.currencyExchangeResult;
