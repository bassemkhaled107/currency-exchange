import { createFeatureSelector } from "@ngrx/store";
import * as fromRouter from "@ngrx/router-store";

import * as fromSharedModels from "../../shared-models";
import * as fromCurrencyExchangeReducer from "./currency-exchange.reducer";
import * as fromCurrenciesReducer from "./currencies.reducer";

export interface IRootState {
  currencyExchange: fromCurrencyExchangeReducer.ICurrencyExchangeState;
  currencies: fromCurrenciesReducer.ICurrenciesState;
  router: fromRouter.RouterReducerState<fromSharedModels.RouterStateUrl>;
}

export const reducers = {
  currencyExchange: fromCurrencyExchangeReducer.reducer,
  currencies: fromCurrenciesReducer.reducer,
  router: fromRouter.routerReducer,
};

export const getCurrencyExchangeState =
  createFeatureSelector<fromCurrencyExchangeReducer.ICurrencyExchangeState>(
    "currencyExchange"
  );

export const getCurrenciesState =
  createFeatureSelector<fromCurrenciesReducer.ICurrenciesState>("currencies");

export const getRouterState =
  createFeatureSelector<fromSharedModels.RouterStateUrl>("router");
