import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { map, catchError, tap, switchMap } from "rxjs/operators";
import { of } from "rxjs";
import * as fromSharedServices from "../../shared-services";
import * as fromActions from "../actions";

@Injectable()
export class CurrenciesEffects {
  constructor(
    private actions$: Actions,
    private currenciesService: fromSharedServices.CurrenciesService
  ) {}

  loadCurrencies$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.loadCurrencies),
      switchMap(({}) =>
        this.currenciesService.loadCurrencies().pipe(
          map((data) => fromActions.loadCurrenciesSuccess({ payload: data })),
          catchError((error) =>
            of(
              fromActions.loadCurrenciesFailure({
                payload: error && error.error,
              })
            )
          )
        )
      )
    )
  );
}
