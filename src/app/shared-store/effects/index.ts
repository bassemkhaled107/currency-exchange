import { CurrenciesEffects } from "./currencies.effects";
import { CurrencyExchangeEffects } from "./currency-exchange.effects";

export const EFFECTS: any = [CurrencyExchangeEffects, CurrenciesEffects];

export { CurrenciesEffects } from "./currencies.effects";
export { CurrencyExchangeEffects } from "./currency-exchange.effects";
