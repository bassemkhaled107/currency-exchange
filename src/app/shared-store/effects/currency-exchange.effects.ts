import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { map, catchError, tap, switchMap } from "rxjs/operators";
import { of } from "rxjs";
import * as fromSharedServices from "../../shared-services";
import * as fromActions from "../actions";

@Injectable()
export class CurrencyExchangeEffects {
  constructor(
    private actions$: Actions,
    private currencyExchangeService: fromSharedServices.CurrencyExchangeService,
    private snackBar: MatSnackBar,
    private router: Router
  ) {}

  currencyExchange$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.currencyExchange),
      switchMap(({ payload }) =>
        this.currencyExchangeService
          .currencyExchange(payload.baseCurrency, payload.convertToCurrency)
          .pipe(
            map((data) =>
              fromActions.currencyExchangeSuccess({ payload: data })
            ),
            catchError((error) =>
              of(
                fromActions.currencyExchangeFailure({
                  payload: error && error.error,
                })
              )
            )
          )
      )
    )
  );

  currencyExchangeFailure$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(fromActions.currencyExchangeFailure),
        tap(({ payload }) => {
          this.snackBar.open("Something went wrong", "", {
            duration: 3000,
          });
          this.router.navigate(["/home"]);
        })
      );
    },
    { dispatch: false }
  );
}
